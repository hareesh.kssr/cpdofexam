package com.agiletestingalliance;

public class MinMax {

	public int foo(int varA, int varB) {
		if (varB > varA) {
			return varB;
		}
		else {
			return varA; 
		}
	}

	
	public String bar(String string) {
		if (string!=null && !string.equals("")) {
			return string;
		}
		if (string!=null && string.equals("")) {
			return string;
		}
		return string;
	}
	

}
