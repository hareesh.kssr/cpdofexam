package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;


public class MinMaxTest {

    @Test
	public void fooWithBGreater() throws Exception {
		assertEquals("fooWithBGreater",4,new MinMax().foo(2,4));
	}
	
	@Test
	public void fooWithAGreater() throws Exception {
		assertEquals("fooWithAGreater",2,new MinMax().foo(2,1));
	}
	
	@Test
	public void barWithString() throws Exception {
		assertEquals("barWithString","test",new MinMax().bar("test"));
	}
}