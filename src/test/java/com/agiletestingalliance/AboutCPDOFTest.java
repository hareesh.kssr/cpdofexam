package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;


public class AboutCPDOFTest {

    @Test
	public void descStringCheck() throws Exception {
	
	    assertEquals("TestWithExactString","CP-DOF certification program covers end to end DevOps Life Cycle practically. CP-DOF is the only globally recognized certification program which has the following key advantages: <br> 1. Completely hands on. <br> 2. 100% Lab/Tools Driven <br> 3. Covers all the tools in entire lifecycle <br> 4. You will not only learn but experience the entire DevOps lifecycle. <br> 5. Practical Assessment to help you solidify your learnings.",new AboutCPDOF().desc());
	}
	
	@Test
	public void descStringCheckNegative() throws Exception {
	
	    assertNotEquals("TestWithExactStringNegative","covers end to end DevOps Life Cycle practically. CP-DOF is the only globally recognized certification program which has the following key advantages: <br> 1. Completely hands on. <br> 2. 100% Lab/Tools Driven <br> 3. Covers all the tools in entire lifecycle <br> 4. You will not only learn but experience the entire DevOps lifecycle. <br> 5. Practical Assessment to help you solidify your learnings.",new AboutCPDOF().desc());
	}

}